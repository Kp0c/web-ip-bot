﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace webip_bot
{
    /// <summary>
    /// Класс парсит входящие инструкции методом ParseThisCommand.
    /// </summary>
    class ParseInstructions
    {
        //..................................Delegats.................................

        public delegate void BackContainer();
        public delegate void EndContainer();
        public delegate void GoToContainer(string adress, bool isMemorize);
        public delegate void LogContainer(string log);
        public delegate void WaitContainer(int time);
        public delegate void ClickContainer(string Tag, string Attr, string Equals);
        public delegate void GotoRandomContainer(string[] adresses, bool isMemorize);
        public delegate void GetValueContainer(string Tag, string Attr, string Equals, string ValueAttr);
        public delegate void TempContainer();
        public delegate void CheckContainer();
        public delegate void SetTextContainer(string Tag, string Attr, string Equals, string Text);
        public delegate void MemorizeContainer();
        public delegate void FirstLinkClickOnSiteContainer(bool isMemorize);
        public delegate void gotoDefaultContainer(string site);
        //..................................Events...................................
        /// <summary>
        /// Команда back. Возвращает браузер на 1 страницу назад
        /// </summary>
        public event BackContainer Back;
        /// <summary>
        /// Команда Log. Логирует текст
        /// </summary>
        public event LogContainer Log;
        /// <summary>
        /// Команда End. Конец скрипта
        /// </summary>
        public event EndContainer End;
        /// <summary>
        /// Команда Goto. Переходит по ссылке
        /// </summary>
        public event GoToContainer GoTo;
        /// <summary>
        /// Команда Wait. Ждёт определённое время в мс.
        /// </summary>
        public event WaitContainer Wait;
        /// <summary>
        /// Команда Click. Кликает по ссылке
        /// </summary>
        public event ClickContainer Click;
        /// <summary>
        /// Переходит по рандомной ссылке (рандом надо обрабатывать в вашем коде)
        /// </summary>
        public event GotoRandomContainer GotoRandom;
        /// <summary>
        /// Получает занчение поля
        /// </summary>
        public event GetValueContainer GetValue;
        /// <summary>
        /// Звписать во временную переменную
        /// </summary>
        public event TempContainer Temp;
        /// <summary>
        /// TODO: чекнуть результат работы
        /// </summary>
        public event CheckContainer Check;
        /// <summary>
        /// устанавливает текст в поле
        /// </summary>
        public event SetTextContainer SetText;
        /// <summary>
        /// Запоминает текущую ссылку браузера
        /// </summary>
        public event MemorizeContainer Memorize;
        /// <summary>
        /// Кликает по первой попавшейся ссылке
        /// </summary>
        public event FirstLinkClickOnSiteContainer FirstLinkClickOnSite;
        /// <summary>
        /// Переходит по ссылке в браузере по умолчанию
        /// </summary>
        public event gotoDefaultContainer gotoDefault;
        //............................VARIABLES.......................................
        public string[] commands;
        public string temp;
        //............................................................................
        /// <summary>
        /// Парсит комманду
        /// </summary>
        /// <param name="command">Комманда</param>
        public void ParseThisCommand(string command)
        {
            commands = command.Split(" ".ToCharArray(), 10);
            commands[0] = commands[0].ToLower();

            switch (commands[0])
            {
                case "back": Back(); break;
//......................................................................................
                case "log":
                    if (commands.Length > 2)
                        for (int i = 2; i < commands.Length; i++)
                        {
                            commands[1] += ' ' + commands[i];
                        }
                    Log(commands[1]);
                    break;
//......................................................................................
                case "goto":

                    if (commands[1] == "temp") GoTo(temp, Convert.ToBoolean(commands[2]));
                    else try { GoTo(commands[1], Convert.ToBoolean(commands[2])); }
                        catch (IndexOutOfRangeException ex)
                        {
                            Log(ex.Message);
                            GoTo(commands[1], false);
                        }
                    break;
//......................................................................................
                case "end":
                    End(); break;
//......................................................................................
                case "wait":
                    Wait(Convert.ToInt32(commands[1])); break;
//......................................................................................
                case "click":

                    if (commands.Length > 3)
                        for (int i = 4; i < commands.Length; i++)
                        {
                            commands[3] += ' ' + commands[i];
                        }
                    Click(commands[1], commands[2], commands[3]);
                    break;
//......................................................................................
                case "gotorandom":

                    string[] adresses = new string[commands.Length - 2];
                    for (int i = 1; i < commands.Length - 1; i++)
                    {
                        adresses[i - 1] = commands[i];
                    }
                    GotoRandom(adresses, Convert.ToBoolean(commands[commands.Length - 1]));
                    break;
//......................................................................................
                case "getvalue":

                    if (commands.Length > 5)
                    {
                        for (int i = 5; i < commands.Length - 1; i++)
                        {
                            commands[4] += ' ' + commands[i];
                        }
                        GetValue(commands[1], commands[2], commands[3], commands[commands.Length]);
                    }
                    break;
//......................................................................................
                case "temp":

                    if (commands[1] == "sid")
                    {
                        Temp();
                    }
                    break;
//......................................................................................
                case "check": Check(); break;
//......................................................................................
                case "settext":

                    if (commands.Length > 5)
                    {
                        for (int i = 5; i < commands.Length; i++)
                        {
                            commands[4] += ' ' + commands[i];
                        }
                    }
                    SetText(commands[1], commands[2], commands[3], commands[4]);
                    break;
//......................................................................................
                case "memorize":
                    Memorize();
                    break;
//......................................................................................
                case "firstlinkclickonsite":
                    FirstLinkClickOnSite(Convert.ToBoolean(commands[1]));
                    break;
//......................................................................................
                case "gotodefault":
                    gotoDefault(commands[1]);
                    break;
//......................................................................................
                default:
                    Log("Fail command: "+command);
                    break;
            }
        }
    }
}
