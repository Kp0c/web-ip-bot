﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace webip_bot
{
    public partial class Captcha : Form
    {
        public Captcha()
        {
            InitializeComponent();
        }
        Form1 main;
        private void Captcha_Load(object sender, EventArgs e)
        {
            main = this.Owner as Form1;
            webBrowser1.Navigate(main.CaptchaUrl);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            main.CaptchaReturn = textBox1.Text;
            this.Close();
        }
    }
}
