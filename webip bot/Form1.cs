﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using HtmlAgilityPack;
using System.Threading;

namespace webip_bot
{
    public partial class Form1 : Form
    {
        StreamReader curFile;
        string wmid = "223556426375";
        string pass = "124578";
        string secretWord = "Денис";
        string value;
        ParseInstructions Parse;
        Boolean read;
        LoginState state;
        public string CaptchaUrl;
        public string CaptchaReturn=null;
        Form CaptchaForm;
        Random random = new Random();
        
        enum LoginState
        {
            Login,
            Logined,
        }

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        public void Init()
        {
            state = LoginState.Login;
            Parse = new ParseInstructions();
            webBrowser1.Navigate("http://www.web-ip.ru");
            CaptchaForm = new Captcha();
            CaptchaForm.Owner = this;
            
            
            Parse.Log += Log;
            Parse.Back += Back;
            Parse.GoTo += GoTo;
            Parse.Click += Parse_Click;
            Parse.End += End;
            Parse.Wait += Wait;
            Parse.GotoRandom += GotoRandom;
            Parse.GetValue += GetValue;
            Parse.Temp += Temp;
            Parse.SetText += SetText;
            Parse.Memorize += Memorize;
            Parse.FirstLinkClickOnSite += FirstLinkClickOnSite;
            Parse.gotoDefault += GotoDefault;
            //TODO: Parse.Check += Check;
        }

        void GotoDefault(string site)
        {
            System.Diagnostics.Process.Start(site);
        }

        void FirstLinkClickOnSite(bool isMemorize)
        {
            foreach (HtmlElement he in webBrowser1.Document.Links)
            {
                try
                {
                    if (he.GetAttribute("href").Substring(0, webBrowser1.Document.Url.Host.Length).Equals(webBrowser1.Document.Url.Host))
                    {
                        he.SetAttribute("target", "_top");
                        Log(he.GetAttribute("href"));
                        he.InvokeMember("click");
                        while (webBrowser1.ReadyState != WebBrowserReadyState.Complete) Application.DoEvents();
                        break;
                    }
                }
                catch (Exception) { }
            }
        }

        void Memorize()
        {
            richTextBox2.Text += webBrowser1.Url.AbsoluteUri + "\n";
        }

        void SetText(string Tag, string Attr, string Equals, string Text)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(Tag))
            {
                if (he.GetAttribute(Attr).Equals(Equals))
                {
                    he.InnerText = Text;
                }
            }
        }

        void Check()
        {
            //TODO: check
        }

        void Temp()
        {
            value = null;
            GetValue("input", "name", "sid", "value");
            Parse.temp = value;
        }

        void GetValue(string Tag, string Attr, string Equals, string ValueAttr)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(Tag))
            {
                if (he.GetAttribute(Attr).Equals(Equals))
                {
                   value = he.GetAttribute(ValueAttr);
                }
            }
        }


        
        void GotoRandom(string[] adresses,bool isMemorize)
        {
            GoTo(adresses[random.Next(0, adresses.Length)], isMemorize);
        }

        void Wait(int time)
        {
            Log("wait "+time);
            int ticks = System.Environment.TickCount + time;
            while (System.Environment.TickCount < ticks)
            {
                Application.DoEvents();
            }
        }

        void End()
        {
            read = false;
            richTextBox2.Clear();
            Log("End with this work");
        }

        void Parse_Click(string Tag, string Attr, string Equals)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(Tag))
            {
                try
                {
                    if (he.GetAttribute(Attr).Substring(0, Equals.Length).Equals(Equals))
                    {
                        Log("Click tag " + Tag + " attr " + Attr + " contains " + Equals);
                        he.SetAttribute("target", "_top");
                        he.InvokeMember("click");
                        while (webBrowser1.ReadyState != WebBrowserReadyState.Complete) Application.DoEvents();
                        break;
                    }
                }
                catch (Exception) { }
            }
        }

        void GoTo(string adress, bool isMemorize)
        {
            webBrowser1.Navigate(adress);
            if (isMemorize) richTextBox2.Text += adress+"\n";
            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete) Application.DoEvents();

        }

        void Back()
        {
            if (webBrowser1.CanGoBack) webBrowser1.GoBack();
        }

        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        public void GetNewSimpleJob()
        {
            List<FileInfo> files = new List<FileInfo>();
            DirectoryInfo dir = new DirectoryInfo(@"Jobs");
            foreach (var item in dir.GetFiles())
            {
                files.Add(item);
            }
            foreach (var item in files)
            {
                read = true;
                curFile = new StreamReader(item.FullName, Encoding.GetEncoding(1251));
                while (read)
                {
                    Parse.ParseThisCommand(curFile.ReadLine());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetNewSimpleJob();
        }

        public void Log(string log)
        {
            richTextBox1.Text += log + "\n";
        }


        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            #region log-in
            if (state == LoginState.Login)
            {
                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                {
                    if (he.GetAttribute("name").Equals("wmid"))
                    {
                        he.InnerText = wmid;
                    }
                    if (he.GetAttribute("name").Equals("pass"))
                    {
                        he.InnerText = pass;
                    }
                }
                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                {
                    if (he.GetAttribute("value").Equals("Войти"))
                    {
                        he.InvokeMember("click");
                        Log("Enter with wmid " + wmid + " and pass " + pass);
                    }
                }
                state = LoginState.Logined;
            }
            #endregion

            #region need capcha?
                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("img"))
                {
                    if (he.GetAttribute("title").Equals("Обновить изображение"))
                    {
                        Log("Enter capcha, please");
                        CaptchaUrl = he.GetAttribute("src");
                        
                        CaptchaForm.ShowDialog();
                        if (CaptchaReturn!=null)
                        {
                            foreach (HtmlElement he2 in webBrowser1.Document.GetElementsByTagName("input"))
                            {
                                if (he2.GetAttribute("name").Equals("kanswer"))
                                {
                                    he2.InnerText = secretWord;
                                }
                                if (he2.GetAttribute("name").Equals("cnum"))
                                {
                                    he2.InnerText = CaptchaReturn;
                                }
                                if (he2.GetAttribute("value").Equals("Далее"))
                                {
                                    he2.InvokeMember("click");
                                }
                            }
                            CaptchaReturn = null;
                        }
                    }
                }
            #endregion

            #region Проверяем баланс
                if (webBrowser1.Document.Url.Host == "www.web-ip.ru")
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("font"))
                    {
                        if (he.InnerText.Contains("руб.") && he.Parent.GetAttribute("colspan").Equals("2"))
                        {
                            label3.Text = he.InnerText;
                        }
                    }
                }
            #endregion
        }
    }
}
